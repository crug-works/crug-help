# CRUG Help

![CRUG logo](crug_skyline.png)

## Repository for CRUG help documents, tutorials, and vignettes to support any useR from beginners to experts:

- ### Bug Reporting
- ### Conda Environment Setup
- ### Compile and Installs
- ### Docker Builds
- ### Git Helper Docs
- ### Language Translation
- ### OS Guidelines
- ### Reprex Setup

